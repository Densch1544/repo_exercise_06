﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Spawner : MonoBehaviour
    {

        public int spawnCounter;

        void SpawnLoop()
        {
            for (int i = 0; i < spawnCounter; i++)
            {
                GameObject newGo = new GameObject();
                Debug.Log(message: "The Enemy spawned");
            }
        }

        void OtherSpwanLoop(int turboSpawner)
        {
            while (turboSpawner < spawnCounter)
            {
                GameObject newGo = new GameObject();
                Debug.Log(message: "The other Enemy spawned");
                turboSpawner++;
            }
            
        }

        void AnotherSpawnLoop(int spawny)
        {
            do
            {
                GameObject newGo = new GameObject();
                Debug.Log(message: "Another Enemy spawned");
                spawny++;
            } while (spawny < spawnCounter);
        }



        void Start()
        {
            OtherSpwanLoop(10);
            AnotherSpawnLoop(5);
            SpawnLoop();
        }

    }
}
